WxT Chart Stability

Setup instructions:

1) Install this module
2) Then go to all of your text formats and look for "Charts Compatibility"
3) Put the checkbox for "Charts Compatibility" to on, then review the corresponding options and change them as you wish. Defaults are included.
4) Re-order the filters so that "Charts Compatibility" weight is lighter than the others (drag it to the top). 
  See screenshot for step 4) https://www.drupal.org/files/project-images/Filter_weight.png
5) be sure to press save, repeat steps 2,3,4,5 for all text formats that use ckeditor

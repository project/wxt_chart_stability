<?php

namespace Drupal\wxt_chart_stability\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a 'CompatibilityFilter' filter.
 *
 * @Filter(
 *   id = "wxt_chart_stability_compatibilityfilter",
 *   title = @Translation("Compatibility Filter"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   settings = {
 *     "clean" = "<td>&nbsp;</td>",
 *   },
 *   weight = -99
 * )
 */
class Compatibilityfilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['clean'] = [
      '#type' => 'textfield',
      '#title' => $this->t('String sequence to replace.'),
      '#default_value' => $this->settings['clean'] ?? '<td>&nbsp;<td>',
      '#description' => $this->t('Clean up the above string sequence.'),
    ];
    $form['desired'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Replace with this sequence.'),
      '#default_value' => $this->settings['desired'] ?? '<td></td>',
      '#description' => $this->t('Replace with this unlimited times.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    // @DCG Process text here.
    $clean = $this->settings['clean'];
    $desired = $this->settings['desired'];
    $text = str_replace($clean, "$desired", $text);
    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Configure this carefully, use a string that is an easy match.');
  }

}
